package com.hacka.filipe.testehacka;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

public class Jogo1Activity extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    Usuarios online;
    Button c1;
    Button c2;
    Button c4;

    Button c8;
    Button c16;

    TextView t1;
    TextView t2;
    TextView t4;
    TextView t8;
    TextView t16;

    int ca1;
    int ca2;
    int ca4;
    int ca8;
    int ca16;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jogo1);
        getSupportActionBar().hide();
        firebaseDatabase = FirebaseDatabase.getInstance();

        Intent intent = getIntent();
        online = (Usuarios) intent.getSerializableExtra("Objeto");

        c1 = findViewById(R.id.btnc1);
        c2 = findViewById(R.id.btnc2);
        c4 = findViewById(R.id.btnc4);
        c8 = findViewById(R.id.btnc8);
        c16 = findViewById(R.id.btnc16);

        t1 = findViewById(R.id.txt1);
        t2 = findViewById(R.id.txt2);
        t4 = findViewById(R.id.txt4);
        t8 = findViewById(R.id.txt8);
        t16 = findViewById(R.id.txt16);

        ca1 = 0;
        ca2 = 0;
        ca4 = 0;
        ca8 = 0;
        ca16 = 0;



        c1.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
        c2.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
        c4.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
        c8.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
        c16.setBackground(this.getResources().getDrawable(R.drawable.backcarta));

        t1.setText("0");
        t2.setText("0");
        t4.setText("0");
        t8.setText("0");
        t16.setText("0");


    }

    public void Carta1(View view){

        Log.d("BT1", c1.getBackground().toString()+" AQIO");


        if(ca1==0){
            c1.setBackground(this.getResources().getDrawable(R.drawable.carta1));
            ca1 = 1;
            t1.setText("1");
        }else{
            c1.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
            ca1 = 0;
            t1.setText("0");
        }

    }

    public void Carta2(View view){

        if(ca2==0){
            c2.setBackground(this.getResources().getDrawable(R.drawable.carta2));
            ca2 = 1;
            t2.setText("1");
        }else{
            c2.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
            ca2 = 0;
            t2.setText("0");
        }

    }

    public void Carta4(View view){

        if(ca4==0){
            c4.setBackground(this.getResources().getDrawable(R.drawable.carta4));
            ca4 = 1;
            t4.setText("1");
        }else{
            c4.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
            ca4 = 0;
            t4.setText("0");
        }

    }

    public void Carta8(View view){

        if(ca8==0){
            c8.setBackground(this.getResources().getDrawable(R.drawable.carta8));
            ca8 = 1;
            t8.setText("1");
        }else{
            c8.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
            ca8 = 0;
            t8.setText("0");
        }

    }

    public void Carta16(View view){

        if(ca16==0){
            c16.setBackground(this.getResources().getDrawable(R.drawable.carta16));
            ca16 = 1;
            t16.setText("1");
        }else{
            c16.setBackground(this.getResources().getDrawable(R.drawable.backcarta));
            ca16 = 0;
            t16.setText("0");
        }

    }
    public void Valida(View view){
        if(ca1==1 && ca4==1 && ca8==1){

            int pontos = Integer.parseInt(online.getPontos());
            if(pontos==0){
                pontos =  20;
                if(pontos<100){
                    online.setPontos(""+pontos);
                }else{
                    int nivel = Integer.parseInt(online.getNivel());
                    nivel = nivel + 1;
                    online.setNivel(""+nivel);
                }
            }


            databaseReference = firebaseDatabase.getReference("Usuarios/"+online.getKey()+"/");
            databaseReference.removeValue();

            DatabaseReference myRef = firebaseDatabase.getReference();

            myRef.child("Usuarios").push().setValue(online);


            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setTitle("PARABÉEENS!!! Você Acertou!!!")
                    .setMessage("Agora que completou o jogo 1, você acumulou seus primeiros 20 pontos!!!")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                           Intent it = new Intent(Jogo1Activity.this, InicioActivity.class);
                          // it.putExtra("Objeto", (Serializable)online);
                           startActivity(it);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }
    }

}
