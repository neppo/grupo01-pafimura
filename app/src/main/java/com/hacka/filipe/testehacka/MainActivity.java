package com.hacka.filipe.testehacka;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    EditText Email;
    EditText Senha;
    String em;
    String se;


    FirebaseAuth firebaseAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        firebaseAuth = FirebaseAuth.getInstance();

        Email = findViewById(R.id.email);
        Senha = findViewById(R.id.senha);
        Email.setText("filipesilva1505@gmail.com");
        Senha.setText("123123123");
    }

    public void Entrar (View view){
        em = Email.getText().toString();
        se = Senha.getText().toString();

        Log.d("MENSAGE", "AQUI");
        firebaseAuth.signInWithEmailAndPassword(em, se).addOnCompleteListener(
                MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            Intent nova = new Intent(MainActivity.this, InicioActivity.class);

                            startActivity(nova);
                        }else {
                            Log.d("MENSSAGE", "ERRO");
                            Context contexto = getApplicationContext();
                            String texto = "E-MAIL OU SENHA INCORRETOS ";
                            int duracao = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(contexto, texto,duracao);
                            toast.show();
                        }
                    }
                });


    }

    public void Cadastrar(View view){
        Intent nova = new Intent(MainActivity.this, CadastrarUsuariosActivity.class);

        startActivity(nova);
    }
}
