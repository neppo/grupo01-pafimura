package com.hacka.filipe.testehacka;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

public class CadastrarUsuariosActivity extends AppCompatActivity {
    EditText nome;
    EditText email;
    EditText senha;

    String nm;
    String em;

    FirebaseAuth firebaseAuth;



    Usuarios novo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_usuarios);
        getSupportActionBar().hide();

        firebaseAuth = FirebaseAuth.getInstance();


        nome = findViewById(R.id.nome);
        email = findViewById(R.id.email);
        senha = findViewById(R.id.senha);

    }

    public void Cadastrar(View view){

        em = email.getText().toString();


        firebaseAuth.createUserWithEmailAndPassword(em, senha.getText().toString())
                .addOnCompleteListener(CadastrarUsuariosActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.d("MENSSAGE", "SUCESSO");

                            nm = nome.getText().toString();

                            novo = new Usuarios(nm, em, "0", "1", "1", "");
                            Intent nova = new Intent(CadastrarUsuariosActivity.this, AvatarActivity.class);
                            nova.putExtra("Objeto", (Serializable) novo);
                            startActivity(nova);

                        }else{
                            Log.d("MENSSAGE", "errooooo" + task.getException());

                        }
                    }
                });



    }
}
