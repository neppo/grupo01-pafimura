package com.hacka.filipe.testehacka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class AvatarActivity extends AppCompatActivity {

    RadioButton menina, menino;
    ImageView imagem;
    EditText nomepersonagem;
    int aux;

    FirebaseDatabase database;
    DatabaseReference databaseReference;

    Usuarios novo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        novo = (Usuarios) intent.getSerializableExtra("Objeto");

        database = FirebaseDatabase.getInstance();
        menina = findViewById(R.id.menina);
        menino = findViewById(R.id.menino);
        imagem = findViewById(R.id.img);
        nomepersonagem = findViewById(R.id.nomePersonagem);
        aux=0;
    }




    public void Alterou(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.menina:
                if (checked)
                    // Pirates are the best
                    imagem.setImageResource(R.drawable.imgm);
                aux=1;
                    break;
            case R.id.menino:
                if (checked)
                    // Ninjas rule
                    imagem.setImageResource(R.drawable.imgh);
                aux=2;
                    break;
        }
    }

    public void Proximo(View view){



        DatabaseReference myRef = database.getReference();
        String  a= aux+"";

        Usuarios salva = new Usuarios(novo.getNome(), novo.getEmail(), novo.getPontos(), novo.getNivel(), a, nomepersonagem.getText().toString());
        Log.d("OOIII", salva.getNomePersonagem() + "aqui");
        myRef.child("Usuarios").push().setValue(salva);

        Intent it = new Intent(this, InicioActivity.class);
        startActivity(it);



    }


}
