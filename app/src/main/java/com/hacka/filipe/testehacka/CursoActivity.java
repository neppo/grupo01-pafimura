package com.hacka.filipe.testehacka;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.Serializable;

public class CursoActivity extends AppCompatActivity {
    Button btnj1;
    Button btnj2;


    Button btnl1;
    Button btnl2;


    Usuarios online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curso);
        getSupportActionBar().hide();


        Intent intent = getIntent();
        online = (Usuarios) intent.getSerializableExtra("Objeto");

        btnj1 = findViewById(R.id.btnj1);
        btnj2 = findViewById(R.id.btnj2);

        btnl1 = findViewById(R.id.btnL1);
        btnl2 = findViewById(R.id.btnL2);

        if("0".equals(online.getPontos())){
            btnj2.setEnabled(false);
            btnj2.setBackground(this.getResources().getDrawable(R.drawable.jdisb));
            btnl1.setEnabled(false);
            btnl1.setBackground(this.getResources().getDrawable(R.drawable.ldisb));
            btnl2.setEnabled(false);
            btnl2.setBackground(this.getResources().getDrawable(R.drawable.ldisb));

        }else{

            int pontos = Integer.parseInt(online.getPontos());
            if(pontos==20){
                btnl1.setEnabled(true);
                btnl1.setBackground(this.getResources().getDrawable(R.drawable.licvt));
                btnj2.setEnabled(false);
                btnj2.setBackground(this.getResources().getDrawable(R.drawable.jdisb));

                btnl2.setEnabled(false);
                btnl2.setBackground(this.getResources().getDrawable(R.drawable.ldisb));

            }else if(pontos==40){
                btnj2.setEnabled(true);
                btnj2.setBackground(this.getResources().getDrawable(R.drawable.icgamer));
                btnl2.setEnabled(false);
                btnl2.setBackground(this.getResources().getDrawable(R.drawable.ldisb));

            }else if(pontos==60){

                btnl2.setEnabled(true);
                btnl2.setBackground(this.getResources().getDrawable(R.drawable.licvt));

            }


        }






    }

    public void Jogo1(View view){
        Intent it = new Intent(this, Licao1Activity.class);
        it.putExtra("Objeto", (Serializable) online);
        startActivity(it);
    }

    public void Jogo2(View view){
        Intent it = new Intent(this, Licao2Activity.class);
        it.putExtra("Objeto", (Serializable) online);
        startActivity(it);
    }

    public void Licao1(View view){
        Intent it = new Intent(this, Conteudo1Activity.class);
        it.putExtra("Objeto", (Serializable) online);
        startActivity(it);
    }
    public void Licao2(View view){
        Intent it = new Intent(this, Conteudo2Activity.class);
        it.putExtra("Objeto", (Serializable) online);
        startActivity(it);
    }
    public void Voltar(View view){
        Intent it = new Intent(this, InicioActivity.class);

        startActivity(it);
    }
}
