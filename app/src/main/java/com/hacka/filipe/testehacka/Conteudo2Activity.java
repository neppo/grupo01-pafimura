package com.hacka.filipe.testehacka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Conteudo2Activity extends AppCompatActivity {
    Usuarios online;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conteudo2);

        firebaseDatabase = FirebaseDatabase.getInstance();

        Intent intent = getIntent();
        online = (Usuarios) intent.getSerializableExtra("Objeto");
    }

    public void Voltar(View view){


        int pontos = Integer.parseInt(online.getPontos());
        if(pontos==60){
            pontos = pontos+20;
            if(pontos<100){
                online.setPontos(""+pontos);
            }else{
                int nivel = Integer.parseInt(online.getNivel());
                nivel = nivel + 1;
                online.setNivel(""+nivel);
                online.setPontos("0");
            }
            databaseReference = firebaseDatabase.getReference("Usuarios/"+online.getKey()+"/");
            databaseReference.removeValue();

            DatabaseReference myRef = firebaseDatabase.getReference();
            myRef.child("Usuarios").push().setValue(online);

        }

        Intent it = new Intent(this, InicioActivity.class);

        startActivity(it);
    }
}
