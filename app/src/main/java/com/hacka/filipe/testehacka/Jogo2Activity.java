package com.hacka.filipe.testehacka;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;

public class Jogo2Activity extends AppCompatActivity {

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    Button b1;
    Button b2;
    Button b4;
    Button b3;
    Button b5;
    Button b6;

    int v1;
    int v2;
    int v3;
    int v4;
    int v5;
    int v6;

    Usuarios online;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jogo2);
        getSupportActionBar().hide();
        firebaseDatabase = FirebaseDatabase.getInstance();

        Intent intent = getIntent();
        online = (Usuarios) intent.getSerializableExtra("Objeto");

        b1 = findViewById(R.id.bt1);
        b2 = findViewById(R.id.bt2);
        b3 = findViewById(R.id.bt3);
        b4 = findViewById(R.id.bt4);
        b5 = findViewById(R.id.bt5);
        b6 = findViewById(R.id.bt6);

        v1 = 0;
        v2 = 0;
        v3 = 0;
        v4 = 0;
        v5 = 0;
        v6 = 0;

        b1.setBackground(this.getResources().getDrawable(R.drawable.br1));
        b2.setBackground(this.getResources().getDrawable(R.drawable.br2));
        b3.setBackground(this.getResources().getDrawable(R.drawable.br3));
        b4.setBackground(this.getResources().getDrawable(R.drawable.br4));
        b5.setBackground(this.getResources().getDrawable(R.drawable.br5));
        b6.setBackground(this.getResources().getDrawable(R.drawable.br6));

    }

    public void Bt1(View view){

        if(v2==0){
            v2=1;
            b2.setBackground(this.getResources().getDrawable(R.drawable.bv2));
        }else {
            v2=0;
            b2.setBackground(this.getResources().getDrawable(R.drawable.br2));
        }

        if(v3==0){
            v3=1;
            b3.setBackground(this.getResources().getDrawable(R.drawable.bv3));
        }else {
            v3=0;
            b3.setBackground(this.getResources().getDrawable(R.drawable.br3));
        }

        if(v6==0){
            v6=1;
            b6.setBackground(this.getResources().getDrawable(R.drawable.bv6));
        }else {
            v6=0;
            b6.setBackground(this.getResources().getDrawable(R.drawable.br6));
        }
        ganhou();

    }

    public void Bt2(View view){


        if(v2==0){
            v2=1;
            b2.setBackground(this.getResources().getDrawable(R.drawable.bv2));
        }else {
            v2=0;
            b2.setBackground(this.getResources().getDrawable(R.drawable.br2));
        }

        if(v3==0){
            v3=1;
            b3.setBackground(this.getResources().getDrawable(R.drawable.bv3));
        }else {
            v3=0;
            b3.setBackground(this.getResources().getDrawable(R.drawable.br3));
        }

        if(v4==0){
            v4=1;
            b4.setBackground(this.getResources().getDrawable(R.drawable.bv4));
        }else {
            v4=0;
            b4.setBackground(this.getResources().getDrawable(R.drawable.br4));
        }

        ganhou();

    }

    public void Bt3(View view){



        if(v1==0){
            v1=1;
            b1.setBackground(this.getResources().getDrawable(R.drawable.bv1));
        }else {
            v1=0;
            b1.setBackground(this.getResources().getDrawable(R.drawable.br1));
        }

        if(v3==0){
            v3=1;
            b3.setBackground(this.getResources().getDrawable(R.drawable.bv3));
        }else {
            v3=0;
            b3.setBackground(this.getResources().getDrawable(R.drawable.br3));
        }

        if(v6==0){
            v6=1;
            b6.setBackground(this.getResources().getDrawable(R.drawable.bv6));
        }else {
            v6=0;
            b6.setBackground(this.getResources().getDrawable(R.drawable.br6));
        }

        ganhou();

    }

    public void Bt4(View view){

        if(v2==0){
            v2=1;
            b2.setBackground(this.getResources().getDrawable(R.drawable.bv2));
        }else {
            v2=0;
            b2.setBackground(this.getResources().getDrawable(R.drawable.br2));
        }

        if(v3==0){
            v3=1;
            b3.setBackground(this.getResources().getDrawable(R.drawable.bv3));
        }else {
            v3=0;
            b3.setBackground(this.getResources().getDrawable(R.drawable.br3));
        }

        if(v6==0){
            v6=1;
            b6.setBackground(this.getResources().getDrawable(R.drawable.bv6));
        }else {
            v6=0;
            b6.setBackground(this.getResources().getDrawable(R.drawable.br6));
        }

        ganhou();


    }

    public void Bt5(View view){

        if(v1==0){
            v1=1;
            b1.setBackground(this.getResources().getDrawable(R.drawable.bv1));
        }else {
            v1=0;
            b1.setBackground(this.getResources().getDrawable(R.drawable.br1));
        }

        if(v3==0){
            v3=1;
            b3.setBackground(this.getResources().getDrawable(R.drawable.bv3));
        }else {
            v3=0;
            b3.setBackground(this.getResources().getDrawable(R.drawable.br3));
        }

        if(v4==0){
            v4=1;
            b4.setBackground(this.getResources().getDrawable(R.drawable.bv4));
        }else {
            v4=0;
            b4.setBackground(this.getResources().getDrawable(R.drawable.br4));
        }

        ganhou();

    }

    public void Bt6(View view){

        if(v1==0){
            v1=1;
            b1.setBackground(this.getResources().getDrawable(R.drawable.bv1));
        }else {
            v1=0;
            b1.setBackground(this.getResources().getDrawable(R.drawable.br1));
        }

        if(v5==0){
            v5=1;
            b5.setBackground(this.getResources().getDrawable(R.drawable.bv5));
        }else {
            v5=0;
            b5.setBackground(this.getResources().getDrawable(R.drawable.br5));
        }

        if(v4==0){
            v4=1;
            b4.setBackground(this.getResources().getDrawable(R.drawable.bv4));
        }else {
            v4=0;
            b4.setBackground(this.getResources().getDrawable(R.drawable.br4));
        }
        ganhou();

    }

    public void ganhou(){
        if(v1==1 && v2==1 && v3==1 && v4==1 && v5==1 && v6==1){
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setTitle("PARABÉEENS!!! Você Acertou!!!")
                    .setMessage("Agora que completou o jogo 2, você acumulou mais 20 pontos, totalizando 60!! Vai ler a lição dois para entender melhor do assunto e ganhar mais 20 pontos!!!")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            int pontos = Integer.parseInt(online.getPontos());
                            if(pontos==40){
                                pontos = pontos+20;
                                online.setPontos(pontos+"");
                                databaseReference = firebaseDatabase.getReference("Usuarios/"+online.getKey()+"/");
                                databaseReference.removeValue();

                                DatabaseReference myRef = firebaseDatabase.getReference();
                                myRef.child("Usuarios").push().setValue(online);

                            }

                            Intent it = new Intent(Jogo2Activity.this, InicioActivity.class);
                            //it.putExtra("Objeto", (Serializable)online);
                            startActivity(it);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

}
