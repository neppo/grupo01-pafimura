package com.hacka.filipe.testehacka;

import java.io.Serializable;

/**
 * Created by Filipe on 24/02/2018.
 */

public class Usuarios implements Serializable{
    private String nome;
    private String email;
    private String pontos;
    private String nivel;
    private String foto;
    private String nomePersonagem;
    private String key;



    public Usuarios(String nome, String email, String pontos, String nivel, String foto, String nomePersonagem) {
        this.setNome(nome);
        this.setEmail(email);
        this.setPontos(pontos);
        this.setNivel(nivel);
        this.setFoto(foto);
        this.setNomePersonagem(nomePersonagem);
    }
    public  Usuarios(){}

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


    public String getNome() {
        return nome;
    }
    public String getNomePersonagem(){
        return nomePersonagem;
    }


    public void setNome(String nome) {
        this.nome = nome;
    }
    public void setNomePersonagem(String nomePersonagem) {
        this.nomePersonagem = nomePersonagem;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPontos() {
        return pontos;
    }

    public void setPontos(String pontos) {
        this.pontos = pontos;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getFoto() {
        return foto;
    }

    @Override
    public String toString() {
        return "Usuarios:" + "\n" +
                "Nome='" + nome + "\n" +
                "Email='" + email+ "\n" +
                "Pontos='" + pontos + "\n" +
                "Nivel='" + nivel+ "\n" +
                "Foto='" + foto+ "\n" +
                "Nome Personagem='" + nomePersonagem +"\n";
    }
}
