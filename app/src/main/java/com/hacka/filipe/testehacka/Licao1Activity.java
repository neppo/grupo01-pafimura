package com.hacka.filipe.testehacka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.Serializable;

public class Licao1Activity extends AppCompatActivity {

    Usuarios online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licao1);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        online = (Usuarios) intent.getSerializableExtra("Objeto");
    }
    public void Jogar(View view){
        Intent it = new Intent(this, Jogo1Activity.class);
        it.putExtra("Objeto", (Serializable) online);
        startActivity(it);
    }
}
