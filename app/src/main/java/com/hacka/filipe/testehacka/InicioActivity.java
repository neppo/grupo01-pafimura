package com.hacka.filipe.testehacka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InicioActivity extends AppCompatActivity {
    ImageView imageView;
    FirebaseUser user;
    String emailUser;
    Usuarios online;
    TextView txt;
    TextView txtPontos;
    TextView nivelUsuario;

    FirebaseDatabase firebaseDatabase;
    FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    List<Usuarios> ListaUsuarios = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        getSupportActionBar().hide();

        imageView = findViewById(R.id.imagemLogin);
        txt = findViewById(R.id.nomeUsuario);
        txtPontos = findViewById(R.id.PontosUsuario);
        nivelUsuario = findViewById(R.id.NivelUsuario);
        firebaseAuth = FirebaseAuth.getInstance();
        if(firebaseAuth.getCurrentUser()!= null){

            user = FirebaseAuth.getInstance().getCurrentUser();
            emailUser = user.getEmail();
            Log.d("OIIAQUI", emailUser);
        }

        FirebaseApp.initializeApp(InicioActivity.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        Query query=null;
        query = databaseReference.child("Usuarios").orderByChild("email");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot objSnapshot:dataSnapshot.getChildren()){
                    Usuarios p = objSnapshot.getValue(Usuarios.class);
                    if(p.getEmail().equals(emailUser)){
                        Log.d("ENTROU", "sim");
                        online = p;
                        p.setKey(objSnapshot.getKey());
                        if(online.getFoto().equals("1"))imageView.setImageResource(R.drawable.imgm);
                        else imageView.setImageResource(R.drawable.imgh);
                        txt.setText(online.getNomePersonagem());
                        txtPontos.setText("Pontos: "+ online.getPontos());
                        nivelUsuario.setText("Nível: "+online.getNivel());
                        Log.d("nomeUSUARIO", online.getNomePersonagem());
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    public void IniciarCurso(View view){
        Intent it = new Intent(this, CursoActivity.class);
        it.putExtra("Objeto", (Serializable) online);

        startActivity(it);
    }

    public void Sair(View view){
        firebaseAuth.signOut();
        Intent it = new Intent(InicioActivity.this, MainActivity.class);
        startActivity(it);
    }
}
